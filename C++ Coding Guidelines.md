# C++ CODING GUIDELINES

Author: Igor Schelle


<!---
1 Introduction, general information

 	1.1 Foreword
 	
 	1.2 How to read the document
 	
2 Rules to keep

 	2.1 Where and when to apply rules
 	
 	2.2 Naming conventions
 	
	 	2.2.1 Content naminq rules (identifies. variables. functions. macros...)
	 	
 	    2.2.2 Formatting naming rules
 	    
	    2.2.3 Namespaces
	    
 	2.3 Formatting of the code
 	
 	2.4 Pre-processor directives
 	
	 	2.4.1 Cross-platform portability
	 	
 	2.5 Rules for header files
 	
     	2.5.1 Header file naming and placing
     	
     	2.5.2 Header file structure
     	
     	2.5.3 Header file includes and namespaces
     	
     	2.5.4 Include ordering in header files
     	
 	2.6 Rules for implementation files (*.cpp) 
 	
        2.6.1 File naming of implementation files.
        
     	2.6.2 Structure implementation files
     	
     	2.6.3 Include ordering in implementation files
     	
 	2.7 Functions and methods
 	
 	2.8 Classes
 	
     	2.8.1 Classes in general
     	
     	2.8.3 Class definition
     	
     	2.8.4 Interface classes
     	
        2.8.5 Keeping implementation private
        
        2.8.6 Structures
        
        2.8.7 Inheritance
        
     2.9 Templates
     
 3  Best practices
 
     3.1 Best practices in general
     
     3.2 Declarations initialization 
     
     3.3 Methods virtual inheritance
     
     3.4 Pointers, references. parameters 
     
     3.5 Other control structures, assignment, visibility
     
     3.6 Exclusivity of including header and source files
     
     3.7 Error handlinq & exceptions
     
     3.8 Type castin
     
     3.9 Logging
     
     3.10 Efficiency vs premature optimization 
     
     3.11 Automated unit tests
     
     3.12 Code Comments
     
4 References

    4.1 Books
    
    4.2 Links
-->

## 1 Introduction, general information
### 1.1 Foreword

moto: *"The only thing better than learning from your mistakes is learning from someone else's mistakes"*

This and any other guidelines has multiple purposes. 

What it is about:
*  Preventing bugs.
*  Increase readability.
*  Help developer to make decision and move on..
*  Taking away opportunity to be inconsistent

What is it usually NOT about:
*  Speed up code
*  Blame someone for braking this rules

This guideline subset of [c++ core guideline](https://github.com/isocpp/CppCoreGuidelines) combined with Thermofisher specific guidelines.

## 1.2 How to read the document

There are sections:

1. Rules: This chapter should read everyone.
2. This chapter is programming: "Best Practise"
3. Next chapter describes, how should programmer write comments and documentation to code.
4. Code examples illustrates all the main concepts in a clear condensed form.
5. Literature


The most important part is the first section about rules.

The rest of the document just shows how to use specific rules and adds more detail, explanations, and advice.

However, if you do not understand or do not agree with something, please consult the explanations before complaining.

"Should" rules are not just recommendations. They should be kept as much as possible, similar to "must" rules.

"Should" rules can be broken with some exceptions that have good justification.

"Must" rules has to be kept always - with no exceptions.


## 2 Rules to keep
### 2.1 Where and when to apply rules.
**You should:**

-	If an old file clearly uses other rules (for example in some old library) and it is consistent, you should keep it's style. Changing it can be considered only if a total refactoring is going to happen.
	
-	If an old file is a complete mess, write the new code using current and valid rules and suggest refactoring to the manager (or product owner).

**You must:**

[CG-11] New files must be written in compliance with current rules.

[CG-12] Always leave changed code cleaner then you've found it.

[CG-13] When adding new code, always look, if some similar function/module/class do not already exist. When need to adopt, refactor (refactoring is part of coding, always think how your code can be reused by others).

[CG-14] When deleting code, always look for unreferenced/unused data types (used by deleted function), header files, structures and delete it as well!

[CG-15] Follow and fix compiler warnings (Also in Jenkins job), static analyser warnings (TICs) 


### 2.2 Naming conventions
#### 2.2.1 Content naminq rules (identifies. variables. functions. macros...)
**You should:**

[CG-20] Only well established abbreviations should be used. If not sure, use full word.

[CG-21] Identifiers as is 'i', 'x', 'y', should be used only in limited local scope in well established context.

[CG-22] Function names should contain a verb.

[CG-23] Variable names should contain a noun.

[CG:24] Only the following characters should be used for identifiers: a-z, A-Z, 0-1

[CG:25] Underscore '_' should be used in only macros.

[CG:26] Names differing only by similar characters like big O vs zero. O-0-D, I-1-|, S-5, Z-2, n-h, etc should be avoided.

Good example:

```cpp
VellNamedFunction ( );
```

vs bad example: 

```cpp
p00rNam3d_function( ) ; clazz  
```

[CG-27] You should not use a name, which would hide identifier declared in outer scope.

[CG-27a] Special characters outside of basic ASCII must not be used. All unicode and other characters must be encoded or loaded from specialized files.

**You must:**

[CG-29] All identifiers must be derived from English words.

[CG-29] Names must be meaningful but as short as possible.

#### 2.2.2 Formatting naming rules
**You should:**

[CG-30] Namespaces: use namespace to express logical structure. 


Example:


```cpp
namespace foo
{
    namespace bar
    {
        
    }//namespace bar
    
} //namespace foo


```
Example c++ 17

```cpp
namespace foo::bar
{

} //namespace foo::bar


```
[CG-31] Typedefs: `typedef std::string MyString;`

[CG-32] Classes: `class ClassName {};`

[CG-33] Interfaces: `class IMyInterfaceName {};`

[CG-34] Methods & funcions: `Type methodName(Type parameterName) ;`

[CG-35] Local variables: `Type variableName;`

[CG-36] Parameters: `Type variableName;`

[CG-37] Struc member variables: `Type variableName;`

[CG-38] Class member variables: `Type variableName;`

[CG-39] Constants: `const Type constantVariable;`

[CG-40] Enums: `enum class Color { Red, Green, Blue }; `

[CG-41] Static `variables: static Type variableName; // but try to avoid this`

[CG-42] Global `variables: Type globalVariable; // but try to avoid this`

[CG-43] Macros: `#define MY MACRO NAME (arg) ( (arg) * (arg) ) // but try to avoid this`


#### 2.2.3 Namespaces
**You should:**

[CG-50] Use at least one namespace every where

[CG-51] Use at most two namespaces, Remeber that Class, struct and enum class are also sort of namespace

[CG-52] Use short and well named identifiers for namespaces

[CG-53] Do not indent content of namespaces

### 2.3 Formatting of the code

See also additional rules for formatting of namespaces, pre-processor directives, rules for header files.

**You should:**

[CG-60] Indent nested blocks by 4 spaces. No tabs! (Warning: Default MSVC settings produce invisible tabs! Check all your editors on all platforms!)

[CG-62] Place curly brackets on a new line and indented as the previous line.

[CG-63] Put curly braces even around one line blocks.

[CG-65] Put One space character between if, for, while... and brackets: if (condition)

[CG-66] Put spaces around binary operators: 

```cpp
x = (a + b) * c;
```

[CG-67] Keep maximal number of characters per line at maximum 120.

[CG-68] Put space characters between type and * or & : - good example: 

```cpp
float *var; 
std::string &str; 
```

[CG-69] Don't put spaces around member access operators - good example: 
```cpp
object.method( ) ; or object->method( ) ; 
```

[CG-70] Dont put spaces after unary operators: good example: 
```cpp
auto enabled = !valid;
```

**You must:**
[CG-64] Put each statement on a separate line


### 2.4 Pre-processor directives

see also additional rules for include-uniqueness

**You should:**

[CG-80] Use Macros just occasionally and only when really needed. Use const variables, templates, lambdas... instead. 

[CG-81] If you have to use `ifdef `in code, try to move that code to separate function.

[CG-82] Using multiple `ifdef` should be indented, but always start from O column.

[CG-79] Use angle brackets notation in includes `#include `for 3rd party libs and system libs (angle brackets for non active directory includes, quotas only for current directory). 

[CG-88] Macros are allowed for conditional compilation. For example in logger where macros like LOG DEBUG can be easily removed from release build. 

**You must:**

[CG-83] `#ifndef NDEBUG` to be used for code cornpiled only in debug. (to be investigated why not to use `#ifdef DEBUG` instead). 

[CG-84] `#include `has to be used only for headers at the beginning of the file. 

[CG-85] Use only forward slash / to separate folders in includes: 

```cpp
 #include <LibraryName/MyHeader.h > 
 ```
 
 Reason: Backward slashes \ do not compile on other platforms than Windows! 
 

[CG-86] Do not use `#define` for functions. 

[CG-87] Do not use `#define` for constants - use const variables instead.

[CG-88] Avoid use negative `#ifndef`. It can be overseen.


#### 2.4.1 Cross-platform portability

**You should:**

[CG-91] If you use preprocessor to compile code for various platforms, keep the same order of blocks.

[CG-92] `#ifdef WIN32 `to be used for Windows specific code. 

[CG-93] `#ifdef APPLE` to be used for Apple (iOX, OSX) specific code.

[CG-94] `#ifdef	linux` to be used for Linux specific code.

### 	2.5 Rules for header files
####     	2.5.1 Header file naming and placing

**You should:**

[CG-100] File should be named as the contained interface. If it contains 

```cpp
class EasyLoader 
{
    
    
};
```
, name it EasyLoader.h. Always use *.h extension.  


[CG-101] Library interface headers should be placed in a sub-folder of the same name as the library: 
/LibraryName/LibraryName/IMyHeader.h 

[CG-102] Non-public headers should be placed together with the implementation in the src subfolder:
/LibraryName/LibraryName/IMyHeader.h 

#### 2.5.2 Header file structur

**You should:**

[CG-110] `#prgma once ` should be used instead of macro guards

[CG-111] If you really have to use guards, they should be in the format  ` #ifdef TF_LIBRARY_PATH_FILENAME_H.` 

[CG-112] One header should contain one interface, eventually with several small tightly related functions, constants, structs etc, but do not let it overgrow or spread to wide! 

[CG-113] Only the interface should be defined in the header. Keep implementation where it belongs - in cpp. 


#### 2.5.3 Header file includes and namespaces

**You should:**

[CG-120] Use same principle of *Ordering of includes* as in implementation files. 

[CG-121] Use forward declaration for types mentioned only by pointer or reference instead of including the full header. 

[CG-122] Avoid putting `using namespace TheNamespace ; ` in cpp files. Use it only in limited scope not in whole namespace. 


Reason: Quicker rebuild thanks to fewer includes. A changed header will force rebuild of every file that includes it, even indirectly. 

Note: Private implementation PIMPL can also help, but only when considering members, not interface. 

**You must:**

[CG-122] Avoid putting `using namespace TheNamespace ; ` in headers. 

[CG-123] Avoid putting unnecessary includes into header files! Keep only those necessary to compile the header. 

#### 2.5.4 Include ordering in header files

**Yuo Should**

[GC-129] Use precompiled headers

**You must:**

[GC-130] Follow order of includes and divide into following blocks: 

1. Include the implementation's interface. 

2. System headers in angle brackets. 

3. Libraries in angle brackets including library name

4. Application or library specific headers in quotes with paths.  

[GC-131] Each block must be alphabetically ordered. 

[GC-132] Dont add excessive amount of additional include folders to projects. Only one folder for one fibrary. 

Example of include ordering:

```cpp

#include "Mylnterface. h" 

#include <string> 
#include <vector>

#include <LibraryOne/LibHeader.h>
#include <LibraryTwo/LibHeader.h>

#include "Foo.h"
#include "Bar.h"
#include "AppFolder/Motor.h"
#include "AppFolder/Camera.h"
#include "Common/Base.h"
#include "Common/Types.h"

```

### 2.6 Rules for implementation files (*.cpp) 

#### 2.6.1 File naming of implementation files.

**You must:**

[CG-144] File (cpp) has to be named as the file of the interface it is implementing. If it's implementing ``` MySuperName.h ```, name it ``` MySuperName.cpp ```. 

#### 2.6.2 Structure implementation files

[CG-150]  Order of functions must be the same as in the header. 


#### 2.6.3 Include ordering in implementation files

[CG-160] Use same include orderingrules as for header files

Example of include ordering in CPPs:

```cpp

#include "Mylnterface. h" 

#include <string> 
#include <vector>

#include <LibraryOne/LibHeader.h>
#include <LibraryTwo/LibHeader.h>

#include "Foo.h"
#include "Bar.h"
#include "AppFolder/Motor.h"
#include "AppFolder/Camera.h"
#include "Common/Base.h"
#include "Common/Types.h"

```

### 2.7 Functions and methods

**You should:**

[CG-170] Keep functions small. (maximum between 10-20 of code lines). 

[CG-171] If you see repeated code, move it to separate function.

[CG-172] Always put meaningful name to function. If you don't have right abbreviation, use full name. example: 

```cpp
OpenGripper();  //Good
OpGrp( );       //Bad
```


[CG-173] Each function shall do one thing, And one thing ONLY. Always think, how it can be unit-tested.

[CG-174] Dont exceed flow control (cyclomatic complexity) of a method by 20 independent paths or not more than 3 indents (nested blocks). 

[CG-176] Release all resources when leaving a block - local dumb pointers, locks, ... Especially if you need to use multiple returns in a function.

[CG-181] Write functions from top to bottom of file based on their level of abstraction. Top level of abstraction is at the top of the file (most important or interfaces).

[CG-182] Use maximally 4 arguments in functions.

[CG-175] Avoid use of recursion, if possible. 

[CG-177] Multiple break and continue in loops should not be used. 

**You must:**

[CG-178] If ideal number of code lines cannot be applied (10-20), prepare reason for longer functims.  

[CG-179] If rule [CG-182] cant be applied, hard limit of maximal number of method arguments not more than 7, Anyway prefere to abstract parameters to contextual structure.

[CG-180] All switches which do not handle all enum values must have a default clause with adequate error handling. If default behavior is nothing, clearly state it in a comment.

[CG-181] If you use fallthrough in switch statement. Make it explicit by using atribute: example

```cpp 
    switch(bar)
    {      
        case foo::red:
        break;
        case foo::green:
        break;
        case foo::black:
        [[fallthrough]];
        default:
        break;
    }

```


[CG-183] Use `nullptr` instead of `NULL`

[CG-181] Pointer or reference to a non-static local object must not be returned.


### 2.8 Classes

#### 2.8.1 Classes in general

**You should:**

[CG -185] Keep the class implementation small. Each class should have just single responsibility. Big classes is sign of aggregation of responsibilities.

[CG -186] Write cohesive classes. The more each method of the class uses all variables, the more is the class cohesive.

- if we have groups of methods which use groups of variables -> split it. 

- high cohesion means many small classes.

- often splitting even a large method can lead to creation of several small classes with instance variables. 

- while refactored code is often longer than original, it takes less time to read and understand it. 

#### 2.8.3 Class definition

**You should:**

[CG -191] Interface in headders should expose only the interface itself. Keep all implementation detail in cpp file. Consider pimpl.

[CG -192] Use setters and getters to acces members.

[CG -193] For getters of bool member use keword "is" 'bool isEnabled()'

[CG -194] Dont create unnecesaary default constructors. They usualy lead to partially initialized objects.

[CG -199] 

**You must:**

[CG -195] All classes must declare virtual destructor. 

[CG -196] All acquired resources must be released in destructor. (When you use containers, srnart pointers and other RAII objects, you don't have to use destructors at all!)

[CG -197] 'override' must be used on all overridden virtual methods.

[CG -198] Use public, protected, private order.

[CG -200] Declare first functions then members.

Override Example:

```cpp

// Use the -override- keyword . 
// Compiler will warn you if the metod ch@nges in . 
// Do not add -virtual - in front of overriden methods - override is clearer and more 

class A 
{
    virtual void method();
}

class B: public A 
{
    void method( ) override;            // Good - short as possible, good checking. 
    virtual void method ( ) ;           // Bad - virtual is nor necessary nor checking error 
    virtual void method ( ) override;   // Bad - too long.
};

```

#### 2.8.3 Class definition

**You should:**

[CG-210] Constructor initializers should be each on a new line, indented, start with comma. 

[CG-211] Simple member initialization should be done in initialization list rather then in the body. 

**You must:**

[CG-212]  Member functions and static function of the class has to be in the same order as in the header

[CG-213] Complex initialization must be exception-safe.

[CG-214] Members are initialized in the order in which they are declared (in header), not as they are ordered in the list! Therefore members must be listed in tie same order to avoid confusion.

[CG-215] Avoid invoking virtual methods in constructor or destructor.

Class Constructor Example:

```cpp

DescriptiveName::DescriptiveName (Tl p1, T2 p2)
    :BaseClassConstructor(p1)
    ,variable(p2)
    ,rawPtr1(nullptr)
    ,rawPtr2(nullptr) 
{
    // Throw safe code 
    // -if something throws here and it's not catched, 
    // the destructor of this class is NOT called 
    // - this is especially true for allocating to raw pointers 
    // - use smart pointers or catch and delete try 
    try
    {
        rawPtr1 = new Type[N]; 
        rawPtr2 = new Type[M]; 
        // More stuff to do. . . 
    }
    catch(...)
    {
        // Release in inverse order of allocation. 
        // If any allocation has thrown, rawPtrs were first 
        // initialized to nullptr and therefore delete is safe. delete rawPtr2 ; delete rawPtr1 ; 

        delete [] rawPtr2;
        delete [] rawPtr1;

        // Rethrow because we did not construct the object. throw ; 
        throw std::exception{"Oooops"};
    }
} 

```

#### 2.8.4 Interface classes

**You should:**

[CG-220] Names of the methods should be descriptive enough, so multiple interface will not conflict. 

Good example:

```cpp
ICamera->InitAcquisition();
```

Bad example:

```cpp
ICamera->Init();
```

**You must:**

[CG-221] Interface has to be a class with only pure virtual public functions

[CG-222] Each interface has to define a virtual destructor. 

Example  Class interface:

```cpp
class INiceInterfaceName 
{
public: 
 virtual void Foo(Type param) = 0; 
 virtual ~INiceInterEaceName ( ) { }  
};

```

#### 2.8.5 Keeping implementation private

**You should:**

[CG-225] Avoid Implementation in headers (even constructors getters and setters) as much of the implementation specific data types and helper methods as possible should be placed in the cpp. This is especially good if the class uses a lot of various types to store it's data internally, which would be included everywhere together with it even if nobody wants to know the impmentation. 

[CG-226] Avoid hiding for performance critical operations.


#### 2.8.6 Structures

**You should:**

[CG-230] Common practice is that structures should be used for simple data types where are directly accessible without any logic in methods. (AVR65, AVR66, AVR67, Google). C++ treats structures nearly the sane as classes [see differences](https://www.geeksforgeeks.org/structure-vs-class-in-cpp/), but we can use the distinction for readability.

[CG-231] Define or disable constructor, copy-constructor in structure. Sometimes undefined constructors lead to unexpected behavior (shallow copy vs deep copy). 

Example Structure:

```cpp
struct  Vector3d 
{
    float x{}; 
    float y{}; 
    float z{}; 
}
```


#### 2.8.7 Inheritance

**You should:**

[CG-240] Adding abstract interfaces is not considered as multiple inheritance - you can add how many you like

[CG-241] At most single based class should be used. Use multiple inheritance only when you know, what you are doing.


<!---

Example Inheritance:


```cpp
// TODO add one
```
-->

### 2.9 Templates
####   Overview

Templates are compile time mechanism and arefaster than virtual functions and provided different kind of flexibility

[CG -250] You can use 'typedef' for specialized (more complex) type like ``` typedef std::map<int, std::vector<std::string>>  SuperName; ``` if needed.
  
[CG -251] Template header files can be divided into interface definition and implementation. Since templates must be fully defined in headers, the implementation part can be included into the interface header file, so the interface stays clear. 
 
[CG -253] Use ``` template <typename T> ```on separate row in all cases.

**You must:**

[CG -254] Use ``` typename ``` in templates instead of ``` class ``` - often the type is not a class.

[CG -255] Tests for templates must be created for all actual instantiations.

Example templates:

```cpp
template <typename> Max(T a, T b) 
{
    return a > b ? a  :	b; 
}


template <typename T>
struct Vector3d 
{
    T x{};
    T y{};
    T z{};
    
};

```

## 3  Best practices

Chapter is covering common practices, which need to be kept. It is not focused to given area of interest, rules are rather general

###     3.1 Best practices in general

**You should:**

[BP- 10] Avoid using code smell. See [article](https://arne-mertz.de/2017/08/code-smells-short-list/) about code smell. 

[BP- 11] Avoid using design [anti-pattems](https://sourcemaking.com/antipatterns).

###     3.2 Declarations initialization 

**You should:**
[BP-48] Prefer curly braces {} initialization over alternatives unless you have a strong reason not to. (Reason)[https://www.modernescpp.com/index.php/initialization] example:

```cpp 

  int i{};                // i becomes 0
  auto a{1U};             // a becomes unsigned int

  // Initializations of an arbitrary object using public attributes	
  MyClass myClass{2011,3.14};  


```

[BP-49] Prefere `using` over `typedef` [because using is more flexible](https://www.internalpointers.com/post/differences-between-using-and-typedef-modern-c)

[BP-50] Declarations should be at the smallest posible scope

[BP-51] Using ` const ` is preferred wherever possible - for methods and variables. Even simple parameter types shall be ``` const ```. Don't forget to use ``` const ``` methods.

[BP-52] Use ``` using ``` for templated types:  ```using SuperName = std::map<int, std::vector<std::string>>; ```

[BP-53] Also use ``` using ```  for function pointers

[BP-54] Use ``` std::function<T> ``` for callbacks. It can easily handle calls even to instance's methods

[BP-55] Use one variable declaration per line.

[BP-56] All declarations at file scope should be static where possible. (important, see explanation) 

[BP-57] Use [RAII](https://en.cppreference.com/w/cpp/language/raii) instead of Init methods etc. (or call private Init methods in constructor, if initialization goes too long) 

[BP-58] Wrap into the object, if there is possibility of leaking resource. Resource has be wrapped to an object which will release it in it's destructor. 

[BP-59] Use floating point types wisely. double should be default float type. always consider if less precise float is good enough

[BP-60] Avoid declaring a class, structure, or enumeration in the definition of its type. Do not create unnamed data types.

[BP-61] Dont define own operators that do something unexpected or inconsistent.


**You must:**

[BP-62] Static class members (essentially the same as global objects) must not be used.

[BP-63] Constructors with one parameter has to be declared as explicit

[BP-64] Default copy constructor & copy operator has to be checked if they are safe. Be careful with pointers! If it is required and safe, use the compiler generated

[BP-65] Avoid using numeric values & strings in the code. Use a clear named constant. 

[BP-67] Global variables (especially objects with complex constructors) must not be used.

###     3.3 Methods, virtual inheritance

[BP-70] There should be no unreachable code. Delete it.

[BP-71] Non-virtual methods should not be overridden in derived class

<!---
[BP-72]  (Virtual base classes should not be used. (related to multiple inheritance, do not confuse with normal virtual methods). See Virtual inheritance article
-->

[BP-73] Friend methods should not be used. Unless there is no other way.

[BP-75] Using Empty() or isEmpty() methods are strongly preffered. 
Good example:

```cpp
if(myContainer.isEmpty())
{
    DoSomething();
}

```

Bad example:

```cpp
if(myContainer..size() == 0)
{
    DoSomething();
}

```

[BP-76] When returning a bool based on an expression, write it as part of the return statement

```cpp

return a == b;

```

Bad example:

```cpp
if(a == b)
{
    return true;
}
else
{
    return false;
    
}

```

**You must:**

[BP-74] Avoid using functions with variable argument (elipsis ). it is unsafe.

```cpp
auto findAverage(int count, ...) -> void
{
}
```

###     3.4 Pointers, references. parameters 

**You should:**

[BP-80] Use almost always auto. [Herb Sutter blog](https://herbsutter.com/2013/08/12/gotw-94-solution-aaa-style-almost-always-auto/)

[BP-81] Use STL containers. Start with vector as it is most universal. Maybe later optimise after measurement.

[BP-81a] When you know number of elements use `array`.  When you need dynaamic container and you know or can guess number of elements. use `your_container.reserve()`

[BP-8ab] Avoid `your_container.clean()` in constructors. Containers are born empty.

[BP-81c] If elements in the container are inserted dinamically at any place. you should prefer list. If you need random access iterator, prefere vector.

[BP-81d] References are prefered over pointers.

[BP-82] Prefere smart pointers over raw pointers. The only exception is when some library function requires that.

[BP-83] Prefere unique pointers and move semantic over shared pointers

[BP-84] Passing arguments into function: Small object by value. (int, double), big types by const (containers, imges, objects) by const reference.

[BP-85] Do not return anything by reference or move (`std::move()`) from function scope. Since c++17 is copy elision guaranteed. (and before c++ 17 almost always);

[BP-86] Do not use `reinterpret_cast<T>()`

**You must:**

[BP-88] Avoido output arguments. If you need to output multiple values use `std::option` custom struct, pair or tuple together with structure binding. Example:

```cpp 
//c++17
//---------------------------------------------------
auto Foo(int a = 0) -> std::optional<int> 
{
    std::optional<int>  ret;
    if(a != 0)
    {
        ret = 42;
    }
    
    return ret;
}

int main()
{
    return Foo().value_or(-1);
}

//---------------------------------------------------
auto Bar() -> std::tuple<int, int, int>
{
    return std::make_tuple<int, int, int>(42, 0, 1);
}

int main()
{
    auto[a,b,c] = Bar();
    return a;
}

//c++11 and older
//---------------------------------------------------
struct Coordinate 
{
    int x{};
    int y{};
    int z{};
};

auto Joo() -> Coordinate
{
    return {42,0,1};
}


int main()
{
    auto coord = Joo();
    return coord.x;
}

````

[BP-89] Prefer` std::string_view` as the argument, rather than `const std::string `& to allow more flexibility to callers:

```cpp

std::vector<std::string> read_until(std::string_view terminator)   // C++17
{
    std::vector<std::string> res;
    for (std::string s; std::cin >> s && s != terminator; ) // read a word
    {
        res.push_back(s);
    }
    return res;
}

```
[BP-90] Dont use numeric values and strings. Use well named constant (preferably in *.cpp if it is implementation detail)

[BP-91] Do not use `std::auto_ptr<T>` since it is deprecated as of C++11. 

###     3.5 Other control structures, assignment, visibility

**You should:**

[BP-90] Conditional expressions should not contain side effects.

[BP-91] Assignment should not be done in conditional expressions. It is difficutl to understand the code.

[BP-92] Value should not be first in conditions `(0 == variable)`. What is tested is more important than the value. 

[BP-93] Don't test bool variables in if condition to equality of true. Good example: `if (variable)` vs bad example `if (variable == true ) `

[BP-94] Avoid using `assert ( ) `

[BP-95] Prefere c++17 If statement with initializer. `if (init; condition)` example:
 
```cpp
if (auto ret = map.insert({ "hello", 3 }); !ret.second)
{
    std::cout << "hello already exists with value " << ret.first->second << "\n";
}
```
**You must:**

[BP-96] Avoid using `goto, setjmp, longjmp` macros. 

[BP-97] Avoid using `abort , exit` (unless it is last command of program). 

[BP-98] Don't test floating point values for exact equality. use range check.

[BP-99] Avoid using `unions`

[BP-100] Digraph and trigraph sequences must not be used. `<%` is equal to `{` , `??=`	is equal to `#` etc.)

###     3.6 Exclusivity of including header and source files

[BP-100] Do not use include guards. Prefere `#pragma once `

bad example:

```cpp

#ifndef FOO_BAR_HEADER_H 
#define FOO_BAR_HEADER_H 
#endif

```
###     3.7 Error handlinq & exceptions
**In general**
Some companies dont use exception, because it has some peerformance penality. We can. So do it.

Error codes are often ignored. Exception are great tool for error handling.

You should:

[BP-109] If some module uses exceptions, it should be used locally (just in this module). Exceptions shouldn't be thrown out of module definition. 

[BP-100] If the method returns error code, it should be tested.

[BP-111] Exceptions are prefered over returning error codes when handling serious problems.

[BP-112] All functions throwing an exception should document this in header

**You must:**

[BP-113] Compile time arrors are preffered over runtime errors


###     3.8 Type casting
**You should:**

[BP-120] Do not use C-style cast. Bad example: 
```cpp
int a{10};
auto b = (short)a;
```
good example:

```cpp
int a{10};
auto b = static_cast<short>(a);
```

The rationale behind is:
1. `static_cast<>()` gives you a compile time checking ability, C-Style cast doesn't.
2. `static_cast<>() `is more readable and can be spotted easily anywhere inside a C++ source code, C_Style cast is'nt.
3. Intentions are conveyed much better using C++ casts.

[BP-122] `dynamic_cast<T>` result should be tested for nullity.

[BP-122] Use `std::dynamic_pointer_cast<T>, std::static_pointer_cast<T>, std::const_pointer_cast<T>` for shared pointers

###     3.9 Logging

**Generally speaking: ** 

Using infra logging is prefered over custom console output


**You should:**


[BP-125] Use infra logging.

[BP-126] Set proper logging severity

###     3.10 Efficiency vs premature optimization 

Prenature optimization causes a lot of problems in programming.

It is one of bad practise known as STUPID principles (Singleton, Tight Coupling, Untestability, **Premature Optimization**, Indescriptive Naming, Duplication )

**You must:**

[BP-130] Use as much compile time evaluation as you can. Use `constexpr` functions

[BP-131a] STL algorithm together with conainers and lambda function should always be first choice. Do not write raw loops!

[BP-131b] Some STL algorihms can work in paralell. Use execution policy  `std::execution::parallel_policy `

[BP-132] Before any optimization always perform measurement.


###     3.11 Parallel programming

**You should:**

[BP-140]Use concurrency to improve responsiveness or to improve throughput.

[BP-141] Prefer packaged_task and futures over direct use of threads and mutexes.

[BP-142] Prefer mutexes and condition_variables over direct use of atomics except for simple counters.

[BP-143] Avoid explicitly shared data whenever you can.

[BP-144] Avoid data races.

[BP-145] Leave lock-free programming to experts.

[BP-146] Do not destroy a running thread.

[BP-147] Use join() to wait for a thread to complete.

[BP-148] Consider using a guarded_thread to provide RAII for threads.

[BP-149] Do not detach() a thread unless you absolutely have to.

[BP-150] Use lock_guard or unique_lock to manage mutexes.

[BP-151] Use lock() to acquire multiple locks.

[BP-152] Use condition_variables to manage communication among threads.

[BP-153] Return a result using a promise and get a result from a future.

[BP-154] Use packaged_tasks to handle exceptions thrown by tasks and to arrange for value return.

[BP-155] Don’t get() twice from a future. Use `std::shared_future<T>` iw you need to get multiple times.

[BP-156] Use async() to launch simple tasks;

[BP-157] Sometimes, a sequential solution is simpler and faster than a concurrent solution. So thing twice when you want to use parallel solution for performance gain.

**You must:**

[BP-158] Keep critical section smallest possible.

###     3.12 Code Comments

**Owerview**


**You should:**


[BP-300] Commenst should never supply well named functions, its argument and local variables. Comments gets obsolete quite fast, so do not overcomment. However explaining your intent is always wellcome.

[BP-301] Each comment should start by a capital letter and be terminated by a dot. 

[BP-302] Comments should be brief and reasonable. Comments like` // This is constructor.` etc. should be avoided. 

[BP-303] Comments in headers should be for the users, comments in implementation for the maintainers. 

[BP-304] Rather than commenting every single line, the code should be readable in the first place! 

[BP-305] Do not comment out code.

[BP-306] You can use keyword TODO(exclusively , not TO DO, 2DO etc.): with link to RTC, Jira or TT


**You must:**

[BP-307] All comments must be English. 

[BP-308] Especially the headers must be commented properly. 

[BP-308] Any limitations or assumptions must be documented in header. e.g // this library is thread safe.

[BP-309] Every file must contain copyright

```cpp
  //
  // Copyright (c) <actual year> by ThermoFisher Scientific
  // All rights reserved. This file includes confidential and proprietary information of ThermoFisher Scientific
  //
```

###     3.13 Automated unit tests
TBD

## 5 Examples
TBD

## 5 References

###   5.1 Books

*  [c++ The Programming Language](https://anekihou.se/programming/2.%20intermediete.pdf) by Bjarne Stroustrup
*  [A Tour of C++](http://shtykov.spb.ru/C++_A_Tour_of_2nd_edition.pdf) by Bjarne Stroustrup
*  [Concurency in action 2nd edition](https://www.amazon.co.uk/Concurrency-Action-2E-Anthony-Williams/dp/1617294691) by Anthony Williams
*  [Effective Modern C++: 42 Specific Ways to Improve Your Use of C++11 and C++14 ](https://www.amazon.co.uk/Effective-Modern-Specific-Ways-Improve/dp/1491903996/ref=sr_1_1?keywords=Effective+Modern+C%2B%2B&qid=1572428898&s=books&sr=1-1) by Scott Mayers
    
###   5.2 Links

* [Google](www.google.com)
* [Cplusplus](http://www.cplusplus.com/)
* [Cppreference](https://cs.cppreference.com/w/)
* [Geeks for geeks](https://www.geeksforgeeks.org/)
* [Simplify c++](https://arne-mertz.de/)
* [Bartek's coding blog](https://www.bfilipek.com/)


<!---
TODO LIST

-establish invariant of clas
x -using auto almos always auto
x-brace initialization
x-do not use raw loops there use algoriths or ranged for loops
x-default container is vector
x-f you know the size use array
x-if you can guess the size use reserve
x-use string view
//-do not use raw pairs always use structured bing
x-do not use reinterpret cast - it is undefined behavior
x-TODO Using
x-TODO STD optional
x-TODO Curly braces initialization
x-vector vs. list
x-parallelism
x-copyright in to the comment?
x-do not use auto_ptr
nepouzivat zbytecne freestore
use enum clases

) 
-->       




